require 'bundler/setup'
require 'sinatra/base'
require 'yaml'

Dir[File.expand_path('./lib/*.rb')].each {|f| require f }


class Application < Sinatra::Base
  root_dir = File.dirname(__FILE__)
  set :environment, ENV['RACK_ENV'] || :development
  set :root, root_dir
  set :app_file, __FILE__
  set :static, true

  yml = File.open(File.join(root_dir, 'fandoms.yml'), 'r')
  splits = YAML.load(yml)
  set :fandoms, splits[0]['fandoms']
  set :vanity_urls, splits[1]['vanity_urls']

  helpers do
    include Helpers
  end

  before do
    @is_mobile = !request.env['X_MOBILE_DEVICE'].nil? && request.env['X_MOBILE_DEVICE'] != 'true'

    if @is_mobile
      settings.fandoms.each_pair {|key, fandom| fandom[:url] = fandom[:mobile] }
      settings.vanity_urls.each_pair {|key, fandom| fandom[:url] = fandom[:mobile] }
    end
  end

  def fanfic_data
    {:fandoms => settings.fandoms, :vanity_urls => settings.vanity_urls}
  end


  get '/mobiletest' do
    if @is_mobile
      "You are accessing this on an #{request.env['X_MOBILE_DEVICE']}."
    else
      "You are accessing this from a computer or an iPad."
    end
  end

  get '/stylesheet.css' do
    sass :stylesheet
  end

  get '/application.js' do
    coffee :application
  end

  get '/about' do
    response.headers['Cache-Control'] = 'public, max-age=' + (3600 * 24 * 365).to_s
    haml :about, :locals => fanfic_data
  end

  get '/advanced' do
    response.headers['Cache-Control'] = 'public, max-age=' + (3600 * 24 * 365).to_s
    haml :advanced_usage, :locals => fanfic_data
  end

  get '/synopsis' do
    response.headers['Cache-Control'] = 'public, max-age=' + (3600 * 24 * 365).to_s
    haml :synopsis, :locals => fanfic_data
  end

  get '/contribute' do
    response.headers['Cache-Control'] = 'public, max-age=' + (3600 * 24 * 365).to_s
    haml :contribute, :locals => fanfic_data
  end

  get '/' do
    response.headers['Cache-Control'] = 'public, max-age=' + (3600 * 24 * 365).to_s
    haml :synopsis, :locals => fanfic_data
  end


  get '/u/:user' do
    if @is_mobile
      redirect "http://m.fanfiction.net/search.php?type=author&ready=1&keywords=#{params[:user]}"
    else
      redirect "http://www.fanfiction.net/~#{params[:user]}"
    end
  end

  vanity_urls.each do |key, fandom|
    get '/' + key.to_s do
      redirect settings.vanity_urls[key][:url]
    end
  end

  # Begin URLs Proper
  get "/:wordcount" do 
    redirect settings.fandoms[:hp][:url].gsub('<num>', params[:wordcount])
  end

  get '/:wordcount/:fandom' do
    redirect settings.fandoms[params[:fandom].downcase.to_sym][:url].gsub('<num>', params[:wordcount])
  end
end
