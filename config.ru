$: << File.join(File.dirname(__FILE__), 'lib')

# Needed to make foreman stop choking.
$stdout.sync = true

require "bundler/setup"
require "rack/mobile-detect"
require "rack/pjax"
require "./application"
use Rack::Pjax
use Rack::MobileDetect

run Application
