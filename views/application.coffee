$ = jQuery

class Site

  constructor: ->
    @initializePjax()
    @initializeGoBox()

  initializePjax: =>
    $(document).pjax('header nav a', '[data-pjax-container]')

  initializeGoBox: =>
    $('#go').change ->
      val = $('#go').val()
      window.location.href = "http://w.prls.co/" + $('#go').val() if val


$ ->
  window.site = new Site

