## Fanfiction.net URL Reductor ##

###Synopsis:###

    http://www.whpff.com/[wordcount](/[fandom name]) 
    e.g http://www.whpff.com/20 (by default, goes to Harry Potter!)

This is a service to simplify the painful process of entering the long, complex FFN URLs on mobile devices and public computers. If you just want to read some fanfic on your iPhone, it's stinking hard to get to the fandom page, and then to set the filters to get the proper length of story.

To use it, you must pass parameters to this URL to receive a proper redirect.

The proper format is 
    
    http://www.whpff.com/[wordcount] 
    http://www.whpff.com/[wordcount]/[fandom_name]

    e.g. 
    http://www.whpff.com/20 
    http://www.whpff.com/40/sw

Wordcount is the number of words multiplied by 1000 — hence a wordcount of '20' will produce stories 20, 000 words longi.
